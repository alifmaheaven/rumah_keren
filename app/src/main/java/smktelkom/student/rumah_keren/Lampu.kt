package smktelkom.student.rumah_keren


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_lampu.*


class Lampu : Fragment() {


    private var mDatabase: DatabaseReference? = null
    private var mMessageReference: DatabaseReference? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view =  inflater.inflate(R.layout.fragment_lampu, container, false)




        mDatabase = FirebaseDatabase.getInstance().reference
        mMessageReference = FirebaseDatabase.getInstance().getReference("rumah/lampu")


        mMessageReference!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val post = dataSnapshot.child("logika").value
                Log.e("hasil tombol lampu : ", post.toString())

                if (post.toString().toBoolean() == true) {
                    buttonHidupLampu?.visibility = View.VISIBLE
                    buttonMatiLampu?.visibility = View.INVISIBLE
                } else {
                    buttonHidupLampu?.visibility = View.INVISIBLE
                    buttonMatiLampu?.visibility = View.VISIBLE
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("The read failed: " + databaseError.code)
            }
        })

        return view

    }



    override fun onResume() {
        super.onResume()


        buttonHidupLampu.setOnClickListener {
            mDatabase!!.child("rumah").child("lampu").child("logika").setValue(false)
        }

        buttonMatiLampu.setOnClickListener {
            mDatabase!!.child("rumah").child("lampu").child("logika").setValue(true)
        }
    }





}
