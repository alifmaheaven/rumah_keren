package smktelkom.student.rumah_keren


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_security.*


/**
 * A fragment with a Google +1 button.
 */
class Security : Fragment() {
    // The URL to +1.  Must be a valid URL.

    private var mDatabase: DatabaseReference? = null
    private var mMessageReference: DatabaseReference? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_security, container, false)



        mDatabase = FirebaseDatabase.getInstance().reference
        mMessageReference = FirebaseDatabase.getInstance().getReference("rumah/bahaya")


        mMessageReference!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val post = dataSnapshot.child("keamanan").value
                val tanggal = dataSnapshot.child("history").child("tanggal").value
               terakhirDeteksi.text = tanggal.toString()


                Log.e("hasil", post.toString())

                if (post.toString().toBoolean() == true) {
                    buttonHidup?.visibility = View.VISIBLE
                    buttonMati?.visibility = View.VISIBLE
                } else {
                    buttonHidup?.visibility = View.VISIBLE
                    buttonMati?.visibility = View.INVISIBLE
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("The read failed: " + databaseError.code)
            }
        })



        return view
    }

    override fun onResume() {
        super.onResume()


        buttonHidup.setOnClickListener {
            mDatabase!!.child("rumah").child("bahaya").child("keamanan").setValue(true)
        }

        buttonMati.setOnClickListener {
            mDatabase!!.child("rumah").child("bahaya").child("keamanan").setValue(false)
        }
    }

}// Required empty public constructor
