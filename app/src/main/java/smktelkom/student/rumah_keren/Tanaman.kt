package smktelkom.student.rumah_keren


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_tanaman.*


/**
 * A fragment with a Google +1 button.
 */
class Tanaman : Fragment() {
    private var mDatabase: DatabaseReference? = null
    private var mMessageReference: DatabaseReference? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_tanaman, container, false)


        mDatabase = FirebaseDatabase.getInstance().reference
        mMessageReference = FirebaseDatabase.getInstance().getReference("rumah/tanaman")

        mMessageReference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val suhu = dataSnapshot.child("kondisi").child("suhu").value
                val kelembapan = dataSnapshot.child("kondisi").child("kelembapan").value
                val otomatisnya = dataSnapshot.child("otomatis").value
                Log.e("suhu", suhu.toString())
                Log.e("kelembapan", kelembapan.toString())
                Log.e("otomatisnya", otomatisnya.toString())

                autoManual?.isChecked = otomatisnya.toString().toBoolean()

                suhuUdara?.text = suhu.toString() + " C"
                kelembapanUdara?.text = kelembapan.toString() + " %"


            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("The read failed: " + databaseError.code)
            }
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        autoManual.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mDatabase!!.child("rumah").child("tanaman").child("otomatis").setValue(true)
            } else {
                mDatabase!!.child("rumah").child("tanaman").child("otomatis").setValue(false)
            }
        }

    }


}
