package smktelkom.student.rumah_keren

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.core.view.View
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    private var mMessageReference: DatabaseReference? = null
    lateinit var coordinatorLayout: CoordinatorLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()


//        Log.e("Tokene", FirebaseInstanceId.getInstance().token)

        tambahTeman.setOnClickListener {
            val intent = Intent(this, ActivityFab::class.java)
            startActivity(intent)
            finish()
        }

        nav_view.setNavigationItemSelectedListener(this)
        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, Lampu()).addToBackStack("fragment").commit()

        if(isOnline() == false){
            Snackbar.make(coordinatorLayout, "Tidak ada koneksi", Snackbar.LENGTH_LONG).show();
        }


    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val manager = supportFragmentManager
        when (item.itemId) {
            R.id.lampu -> {

                manager.beginTransaction().replace(R.id.frameLayout, Lampu()).addToBackStack("fragment").commit()
            }
            R.id.security -> {

                manager.beginTransaction().replace(R.id.frameLayout, Security()).addToBackStack("fragment").commit()
            }

            R.id.tanaman -> {

                manager.beginTransaction().replace(R.id.frameLayout, Tanaman()).addToBackStack("fragment").commit()
            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

}
